import React from 'react'
import { Link, graphql } from 'gatsby'
import Layout from '../components/layout'
import Image from '../components/image'
import SEO from '../components/seo'

const IndexPage = ({ data }) => {
  const dogs = data.allNodeDog.edges.map(item => item.node)
  return (
    <Layout>
      <SEO title="Home" />
      <div style={{ maxWidth: '300px', marginBottom: '1.45rem' }}>
        <Image />
      </div>
      { dogs.map(dog => <p key={dog.title}>{dog.title} - {dog.body.value}</p>) }
      <Link to="/page-2/">Go to page 2</Link>
    </Layout>
  )
}

export default IndexPage

export const query = graphql`
  query {
    allNodeDog {
	  edges {
	    node {
        title
        body {
          value
        }
	    }
	  }
	}
  }
`
